package sample;

import java.util.Scanner;

public class Swap {
	public static void main(String args[]) {
		int a,b,temp;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the first number:");
		a=scan.nextInt();
		;
		System.out.println("Enter the second number:");
		b=scan.nextInt();
		temp=a;
		a=b;
		b=temp;
		System.out.println("After swaps first number will be " +a);
	
		System.out.println("After swaps second number will be " +b);
		scan.close();
	}
}
